package com.example.firstapplication;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class FirstActivity extends ActionBarActivity implements View.OnClickListener {

    private EditText editTextName, editTextAddress, editTextPhone;
    private Button buttonSubmit, buttonNextActivity;
    private TextView textViewDisplay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);

        editTextName = (EditText) findViewById(R.id.edittext_name);
        editTextAddress = (EditText) findViewById(R.id.edittext_address);
        editTextPhone = (EditText) findViewById(R.id.edittext_phone);

        buttonSubmit = (Button) findViewById(R.id.button_submit);
        buttonSubmit.setOnClickListener(this);

        buttonNextActivity = (Button) findViewById(R.id.button_next_activity);
        buttonNextActivity.setOnClickListener(this);

        textViewDisplay = (TextView) findViewById(R.id.textview_display);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_submit:
                displayResult();
                break;
            case R.id.button_next_activity:
                // TODO Go to Second Activity
                break;
        }
    }

    private void displayResult() {
        String name = editTextName.getText().toString();
        String address = editTextAddress.getText().toString();
        String phone = editTextPhone.getText().toString();

        String messageToShow = "Hello " + name + ". So you live in " + address +
                " and I can contact you in " + phone;

        textViewDisplay.setText(messageToShow);
    }
}
